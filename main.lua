-- Position = { int:x , int:y}
local DEBUG = true
local MAP = '<C><P /><Z><S><S L="801" Y="403" P="0,0,0.3,0.2,0,0,0,0" T="0" X="400" H="46" /><S o="415779" L="98" Y="202" P="0,0,0.3,0.2,0,0,0,0" T="12" X="400" H="19" /></S><D /><O /></Z></C>'
local MAP_POS = {x = 400, y = 200}
local admins = {
    Szel = 'Szel'
}

local currentSettings = {
    player = 'Szel',
    debug = true,
    radius = 150,
    item = tfm.enum.shamanObject.anvil,
    isItemGhost = false,
    items = 3,
    stepsPerItem = 3,
    rings = 1
}
    
local settingsModel = {
    player = 'string',
    debug = 'boolean',
    radius = 'number',
    item = 'number',
    isItemGhost = 'boolean',
    items = 'number',
    stepsPerItem = 'number',
    rings = 'number'
}

local players = {}
-- .settings
-- .objects


local balls = {}
local commands = {}
commands['reload'] = function(playerName, cmd, args)
    reloadPlayer(players[playerName])
end
commands['object'] = function(playerName, cmd, args)
    local item = tfm.enum.shamanObject[args[2]]
    if item then
        changeSettings(playerName, 'item', item)    
    else
        changeSettings(playerName, 'item', args[2])
    end
end
commands['ghost'] = function(playerName)
    local player = players[playerName]
    if player then
        player.settings.ghost = not player.settings.ghost
    end
end
commands['items'] = function(playerName, _, args)
    local items = tonumber(args[2])
    if items then
        if items > 100 then
            tfm.exec.chatMessage(playerName .. " ty debilu! Chcesz nas wszystkich zabić?!")
        else
            changeSettings(playerName, 'items', args[2])
        end
    end
end
commands['radius'] = function(playerName, _, args)
    changeSettings(playerName, 'radius', args[2])
end
commands['steps'] = function(playerName, _, args)
    changeSettings(playerName, 'stepsPerItem', args[2])
end
commands['settings'] = function(playerName, cmd, _)
    local settingsString = cmd:sub(9)
    local newSettings = parseSettings(settingsString)
    currentSettings = tableJoin(newSettings, currentSettings)
    setUp()
end
commands['add'] = function(playerName, _, args)
    if admins[playerName] and args[2] then
        addPlayer(args[2])
    end
end
commands['ban'] = function(playerName, _, args)
    if admins[playerName] and args[2] then
        removePlayer(players[playerName])
    end
end
commands['map'] = function(playerName, _, args)
    if admins[playerName] then
        tfm.exec.newGame(MAP)
    end
end

function setUp()
    forEach(balls, function(b) b:remove() end)
    balls = {}
    tfm.exec.newGame(MAP)
    tfm.exec.movePlayer(currentSettings.player, MAP_POS.x, MAP_POS.y)
    local steps = getCirclePoints(currentSettings)
    for i=1, #steps, currentSettings.stepsPerItem do
        table.insert(balls, createFlyingObject(steps, i, currentSettings))
    end
end

function createObjects(settings)
    local steps = getCirclePoints(settings)
    local objects = {}
    for i=1, #steps, settings.stepsPerItem do
        table.insert(objects, createFlyingObject(steps, i, settings))
    end
    return objects
end

function removeObjects(settings, objects)
    if objects then
        forEach(objects, function(b) b:remove() end)
    end
end

function addPlayer(playerName)
    players[playerName] = {
        settings = copyTable(currentSettings),
    }
    players[playerName].settings.player = playerName
    players[playerName].objects = createObjects(players[playerName].settings)
end

function removePlayer(player)
    removeObjects(player.objects)
    player.objects = nil
    player.settings = nil
end

function reloadPlayer(player)
    removeObjects(player.objects)
    player.objects = createObjects(player.settings)
end

function playerExists(playerName)
    return tfm.get.room.playerList[playerName] ~= nil
end

function moveObjects(playerName, objects)
    if playerExists(playerName) then
        forEach(
            objects, 
            function(object) object:move() end)
    end
end

function changeSettings(playerName, settingsType, settingsValue)
    local player = players[playerName]
    if player then
        local val = parse(settingsModel[settingsType], settingsValue)
        if val then
            player.settings[settingsType] = val
        end
    end
end

function createFlyingObject(steps, startStep, settings)
    local o = {}
    o._currentStep = startStep
    o._objId = createObject(settings)
    o._player = settings.player
    o.move = function(self)
        self._currentStep = getNextStepNumber(#steps, self._currentStep)
        local speed = calculateSpeed(settings, steps, self._currentStep)
        local plPos = getPlayerPosition(self._player)
        tfm.exec.moveObject(
            self._objId, 
            steps[self._currentStep].x + plPos.x, 
            steps[self._currentStep].y + plPos.y, 
            false, 
            speed.x, 
            speed.y,
            false)
    end
    o.remove = function(self)
        tfm.exec.removeObject(self._objId)
    end
    return o
end

function getPlayerPosition(name)
    return {
        x = tfm.get.room.playerList[name].x,
        y = tfm.get.room.playerList[name].y
    }
end

function calculateSpeed(settings, steps, stepNo)
    local step = steps[stepNo]
    local nStep = steps[getNextStepNumber(#steps, stepNo)]
    local ratio = 0.33333
    local time = 0.5 -- event loop - 500 ms
    local gravity = 10
    
    local speed = {}
    speed.x = (nStep.x - step.x) * ratio / time
    speed.y = ((nStep.y - step.y) - (gravity / time^2 / 2))  / time * ratio
    if speed.x == 0 then
        speed.x = 1
    end
    if speed.y == 0 then
        speed.y = 1
    end
    return speed
end

function getNextStepNumber(amount, current)
    if current == amount then
        return 1
    end
    return current + 1
end

-- returns object id
function createObject(settings)
    local p = getRandomPoint()
    return tfm.exec.addShamanObject(settings.item, p.x, p.y, 0, 0, 0, settings.isItemGhost)
end

function getCirclePoints(settings)
    local points = {}
    local noOfPoints = settings.items * settings.stepsPerItem
    for i=1,noOfPoints do
        local angle = (math.pi * 2 /noOfPoints) * i
        local point = {}
        point.x = settings.radius * math.sin(angle)
        point.y = settings.radius * math.cos(angle)
        
        print(point.x .. ' ' .. point.y)
        table.insert(points, point)
    end
    return points
end

function parseSettings(input, oldSettings)
    input = input or ''
    local s = {}
    local params = split(input)
    for _,v in ipairs(params) do
        local param = split(v, ':')
        s[param[1]] = param[2]
        if param[1] == 'item' and type(param[2]) == 'string' then
            param[2] = tfm.enum.shamanObject[param[2]]
        end
    end
    return parseModel(settingsModel,s)
end

function parseModel(model, values)
    local parsed = {}
    for k,v in pairs(model) do
        local val = parse(v, values[k])
        if val then
            parsed[k] = val
        end
    end
    return parsed
end

function checkModelValue(model, propName, propValue)
    return model[propName] == type(propValue)
end

-- EVENTS --

function eventLoop()
    forEach(
        players, 
        function(player) moveObjects(player.settings.player, player.objects) end)
end

function eventChatCommand(player, cmd)
    local args = {}
    for w in cmd:gmatch("%S+") do
        table.insert(args, w)
    end
    if commands[args[1]] then
        commands[args[1]](player, cmd, args)
    end
end

function eventNewGame()
    forEach(
        players, 
        function(player) reloadPlayer(player) end)
end

-- HELPERS -- 

function getRandomPoint()
    return {
        x = math.random(800),
        y = math.random(400)
    }
end

function forEach(list, func)
    for _,v in pairs(list) do
        func(v)
    end
end

function debug(message)
    if DEBUG then
        print(message)
    end
end

function tableJoin(t1, t2)
    for k,v in pairs(t1) do
        t2[k] = v
    end
    return t2
end

function parse(type, value)
    if value == nil then
        return nil
    elseif type == 'string' then
        return tostring(value)
    elseif type == 'number' then
        return tonumber(value)
    elseif type == boolean then
        if string.lower(value) == 'true' then
            return true
        elseif string.lower(value) == 'false' then
            return false
        end
    end   
end

function copyTable(t)
  local copy = {}
  for k,v in pairs(t) do
    copy[k] = v
  end
  return copy
end

-- separator must be single character
function split(input, separator)
    input = input or ''
    separator = separator or "%s"
    local t = {}
    local escape = {
        ['\\'] = true, 
        ['['] = true, 
        [']'] = true, 
        ['('] = true, 
        [')'] = true, 
        ['^'] = true, 
        ['.'] = true
       }
    if escape[separator] then
        separator = '\\' .. separator
    end
    for match in string.gmatch(input, '([^' .. separator .. ']+)') do
        table.insert(t, match)
    end
    return t
end

if not tfm.exec.chatMessage then
    tfm.exec.chatMessage = print
end


tfm.exec.newGame(MAP)
forEach(admins, function(name) addPlayer(name) end)
-- run 
--setUp()
 
-- function startTimer()
-- local ticksPerSecond = 12
-- local s = 1000/ticksPerSecond
-- for t = 0, 1000 - s, s do
--     system.newTimer(function ()
--         system.newTimer(loop, 1000, true)
--         end, 1000 + t, false)
--     end
-- end
-- startTimer()